# Connect Four

Simple terminal version of [Connect Four](https://en.wikipedia.org/wiki/Connect_Four) in Python.

```shell
python C4.py
```

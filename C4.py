from Board import Board
from Piece import Piece


def piece_to_text(piece: Piece) -> str:
    if piece == Piece.PLAYER_1:
        return "X"
    elif piece == Piece.PLAYER_2:
        return "O"
    else:
        return "."


def board_to_text(board: Board) -> str:
    text = ""

    # Pieces
    for row_pieces in board.pieces:
        for piece in row_pieces:
            text += piece_to_text(piece) + "  "
        text += "\n"

    # Column numbers
    for column in range(board.columns):
        text += str(column) + "  "

    return text


def ask_column() -> int:
    while True:
        try:
            return int(input("Select column: "))
        except ValueError:
            print("Not a number, try again")


def play():
    board = Board()
    while True:
        for piece in Piece:
            print("\n" + board_to_text(board) + '\n')
            print("Player " + piece_to_text(piece) + " turn!")

            while True:
                column = ask_column()
                if board.add_piece(piece, column):
                    break
                else:
                    print("Not a valid column, try again")

            if board.is_victorious(piece):
                print("\n" + board_to_text(board) + '\n')
                print("Player " + piece_to_text(piece) + " won!")
                return


if __name__ == '__main__':
    play()

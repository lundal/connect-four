from Piece import Piece
from Position import Position


class Board:
    def __init__(self, rows: int = 5, columns: int = 7, consecutive: int = 4):
        self.rows = rows
        self.columns = columns
        self.consecutive = consecutive

        self.pieces = []
        self.reset_pieces()

    def reset_pieces(self) -> None:
        self.pieces = []
        for row in range(self.rows):
            pieces_row = []
            for column in range(self.columns):
                pieces_row.append(None)
            self.pieces.append(pieces_row)

    def add_piece(self, piece: Piece, column: int) -> bool:
        for row in range(self.rows - 1, -1, -1):
            position = Position(row, column)
            if self.is_inside(position):
                if self.get_piece(position) is None:
                    self.set_piece(position, piece)
                    return True
        return False

    def is_victorious(self, piece: Piece) -> bool:
        # Check from every position
        for row in range(self.rows):
            for column in range(self.columns):
                # Check in every direction (Vertical |, Horizontal –, Diagonal \, Diagonal /)
                directions = [Position(1, 0), Position(0, 1), Position(1, 1), Position(1, -1)]
                start = Position(row, column)
                for direction in directions:
                    if self.count_consecutive(piece, start, direction) >= self.consecutive:
                        return True
        return False

    def count_consecutive(self, piece: Piece, start: Position, direction: Position) -> int:
        count = 0
        position = start
        while True:
            # Check if we are outside the board
            if not self.is_inside(position):
                return count

            # Check if it it a different piece
            if self.get_piece(position) != piece:
                return count

            # Increment and continue
            count += 1
            position = position + direction

    def get_piece(self, position: Position) -> Piece | None:
        if self.is_inside(position):
            return self.pieces[position.row][position.column]

    def set_piece(self, position: Position, piece: Piece) -> None:
        if self.is_inside(position):
            self.pieces[position.row][position.column] = piece

    def is_inside(self, position: Position) -> bool:
        return 0 <= position.row < self.rows and 0 <= position.column < self.columns

